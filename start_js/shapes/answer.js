/*WEEK #4 LAB .JS*/

////////////////////////////////////////////////////////////////////////
/*VARIABLES*/

var _divs = document.getElementsByTagName('div');

var _paragraphs = document.getElementsByTagName('p');

var _labels = document.getElementsByClassName('label');

////////////////////////////////////////////////////////////////////////
/* FUNCTIONS */

function makeCircle() {
  for (var i = 0; i<_divs.length; i++) {
  //.length returns the number of items in _divs which
  // is the also the number of items we need to iterate through
    _divs[i].style.borderRadius = '50%';
    //setting border radius to  50% makes a circle
  }
}

function makeSquare() {
  for (var i = 0; i<_divs.length; i++) {
    _divs[i].style.borderRadius = '0';
    //setting border radius to 0% makes a square
  }
}

function makeWhite() {
  for (var i=0; i<_divs.length; i++) {
    _divs[i].style.backgroundColor = 'white';
  }
}

function makeRed() {
  for (var i=0; i<_divs.length; i++) {
    _divs[i].style.backgroundColor = 'red';
  }
}

function makeBlue() {
  for (var i=0; i<_divs.length; i++) {
    _divs[i].style.backgroundColor = 'blue';
  }
}


function toggle() {
  if (_divs[0].style.display == 'none') {
    document.getElementById('hide').innerHTML = 'hide';

    for (var i = 0; i<_divs.length; i++) {
      _divs[i].style.display = 'block';
    }
  }
  else {
    document.getElementById('hide').innerHTML = 'show';
    for (var i = 0; i<_divs.length; i++) {

    _divs[i].style.display = 'none';

    }
  }

}

////////////////////////////////////////////////////////////////////////
/*EVENT LISTENERS*/

document.getElementById('circle').addEventListener('click', makeCircle);

document.getElementById('hide').addEventListener('click', toggle);

document.getElementById('square').addEventListener('click', makeSquare);

document.getElementById('red').addEventListener('click', makeRed);

document.getElementById('blue').addEventListener('click', makeBlue);

document.getElementById('white').addEventListener('click', makeWhite);

document.getElementById('submitAddClass').addEventListener('click', function() {
  var _addClassName = document.getElementById('addClass').value;
  // get the value from the DOM element with ID "addClass" and store it in a
  // variable called _addClassName
  var _targetTag = document.getElementById('tagClassAdd').value;
  // get the value of DOM element with class "tagClassAdd"
  var _newClass = document.getElementsByTagName(_targetTag);
  // use _targetTag variable to get all DOM elements with that matching tag name.
  // Then store this in a variable called _newClass

  for (var i = 0; i<_newClass.length; i++) {
    if (_newClass[i].className != "") {
      _newClass[i].className += (" " + _addClassName);
    }//appends new to the existing class
    else {
    _newClass[i].className +=  _addClassName;
    }//if the element has no class we simly set it = to _addClassName

  }
});

document.getElementById('submitRemoveClass').addEventListener('click',function() {
  var _removeClassName = document.getElementById('removeClass').value;
  var _classRemove = document.getElementsByTagName(document.getElementById('tagClassRemove').value);
  for (var i =0; i<_classRemove.length; i++) {
    _classRemove[i].className = _classRemove[i].className.replace(_removeClassName, "");
  }
});

document.getElementById('submitToggleClass').addEventListener('click', function() {
  var _toggleClassName = document.getElementById('toggleClass').value;
  var _toggleClass = document.getElementsByTagName(document.getElementById('tagClassToggle').value);
  for (var i =0; i < _toggleClass.length; i++) {
    if (_toggleClass[i].className == _toggleClassName) {
      _toggleClass[i].className = _toggleClass[i].className.replace(_toggleClassName, "");
    }
    else {
      if (_toggleClass[i].className == "") {
        _toggleClass[i].className += _toggleClassName;
      }
      else {
        _toggleClass[i].className += (" " + _toggleClassName);
      }
    }
  }
});
