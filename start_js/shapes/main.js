/*WEEK #4 LAB .JS
We've pre-written some of the functions for you in this file.  See how
far you can get without looking at the answer.js file

////////////////////////////////////////////////////////////////////////
VARIABLES
Should have _divs, _paragraphs and  _labels.*/

var _divs = document.getElementsByTagName('div');
var _paragraphs = document.getElementsByTagName('p');
var _labels = document.getElementsByClassName('label')
//selecting elements with div tags for later use





////////////////////////////////////////////////////////////////////////
/* FUNCTIONS
Should have circle,square,red,blue,white and toggle.*/

function makeCircle() {
	for (var i = 0; i<_divs.length; i++) {
	//.length returns the number of items in _divs which
	// is the also the number of items we need to iterate through
		_divs[i].style.borderRadius = '50%';
		//setting border radius to  50% makes a circle
	}
}

function makeSquare() {
	for (var i = 0; i<_divs.length; i++) {
		_divs[i].style.borderRadius = '0%';
	}
}

function makeWhite() {
	for (var i = 0; i<_divs.length; i++) {
		_divs[i].style.background = 'White';
	}
}

function makeRed() {
	for (var i = 0; i<_divs.length; i++) {
		_divs[i].style.background = 'red';
	}
}

function makeBlue() {
	for (var i = 0; i<_divs.length; i++) {
		_divs[i].style.background = 'Blue';
	}
}

function toggle() {
	if (_divs[0].style.display == 'none') {
		document.getElementById('hide').innerHTML = 'hide';

		for (var i = 0; i<_divs.length; i++) {
			_divs[i].style.display = 'block';
		}
	}
	else {
		document.getElementById('hide').innerHTML = 'show';
		for (var i = 0; i<_divs.length; i++) {

		_divs[i].style.display = 'none';

		}
	}

}









////////////////////////////////////////////////////////////////////////
/* EVENT LISTENERS
Should have circle,hide,square,red,blue,white,submitAddClass, submitRemoveClass,
and submitToggleClass.*/

document.getElementById('circle').addEventListener('click', makeCircle);
document.getElementById('square').addEventListener('click', makeSquare);
document.getElementById('white').addEventListener('click', makeWhite);
document.getElementById('red').addEventListener('click', makeRed);
document.getElementById('blue').addEventListener('click', makeBlue);
document.getElementById('hide').addEventListener('click', toggle);
document.getElementById('submitAddClass').addEventListener('click', submitAddClass);
document.getElementById('hide').addEventListener('click', submitRemoveClass);
document.getElementById('hide').addEventListener('click', submitToggleClass);

document.getElementById('submitAddClass').addEventListener('click', function() {
	var _addClassName = document.getElementById('addClass').value;
	// get the value from the DOM element with ID "addClass" and store it in a
	// variable called _addClassName
	var _targetTag = document.getElementById('tagClassAdd').value;
	// get the value of DOM element with class "tagClassAdd"
	var _newClass = document.getElementsByTagName(_targetTag);
	// use _targetTag variable to get all DOM elements with that matching tag name.
	// Then store this in a variable called _newClass

	for (var i = 0; i<_newClass.length; i++) {
		if (_newClass[i].className != "") {
			_newClass[i].className += (" " + _addClassName);
		}//appends new to the existing class
		else {
		_newClass[i].className +=  _addClassName;
		}//if the element has no class we simly set it = to _addClassName
	}
});




